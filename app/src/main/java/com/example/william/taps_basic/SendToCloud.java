package com.example.william.taps_basic;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Toast;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferListener;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferObserver;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferState;
import com.amazonaws.mobileconnectors.s3.transferutility.TransferUtility;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3Client;

import java.io.File;
import java.net.URISyntaxException;

public class SendToCloud extends AppCompatActivity {

    // TAG for logging;
    private static final String TAG = "UploadActivity";
    public static final String COGNITO_POOL_ID = "us-east-1:63f2c1d7-e29a-4d6b-8315-62a04e097332";
    public static final String BUCKET_NAME = "source-folder";
    private TransferUtility sTransferUtility;

    public static final int CAMERA_REQUEST = 6986;
    public static final int GET_IMAGE = 69;
    //public static final int CAMERA_REQUEST = 6986;

    private ImageView place_pic;
    private Button send_to_cloud;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to_cloud);

        Bitmap cameraImage = this.getIntent().getParcelableExtra("data");
        place_pic = (ImageView) findViewById(R.id.pic_to_cloud);
        place_pic.setImageBitmap(cameraImage);
        send_to_cloud = (Button) findViewById(R.id.image_ok);

        CognitoCachingCredentialsProvider sCredProvider = new CognitoCachingCredentialsProvider(
                this,
                COGNITO_POOL_ID,
                Regions.US_EAST_1);

        AmazonS3Client sS3Client = new AmazonS3Client(sCredProvider);

        sTransferUtility = new TransferUtility(sS3Client,
                this);

        send_to_cloud.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent();
                if (Build.VERSION.SDK_INT >= 19) {
                    // For Android versions of KitKat or later, we use a
                    // different intent to ensure
                    // we can get the file path from the returned intent URI
                    intent.setAction(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.putExtra(Intent.EXTRA_LOCAL_ONLY, true);
                } else {
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                }

                intent.setType("image/*");
                startActivityForResult(intent, GET_IMAGE);
            }
        });
    }

    public void return_to_camera_page(View view) {
        Intent retake_pic = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(retake_pic, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap cameraImage = (Bitmap)data.getExtras().get("data");
            place_pic.setImageBitmap(cameraImage);
        }

        if (requestCode == GET_IMAGE && resultCode == RESULT_OK) {
            Uri uri = data.getData();
            String path = null;
            try {
                path = getPath(uri);
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
            beginUpload(path);
            Toast.makeText(this,
                    "Able to get pic.  See error log for details",
                    Toast.LENGTH_LONG).show();
        }
    }

    /*
 * Begins to upload the file specified by the file path.
 */
    private void beginUpload(String filePath) {
        if (filePath == null) {
            Toast.makeText(this, "Could not find the filepath of the selected file",
                    Toast.LENGTH_LONG).show();
            return;
        }
        File file = new File(filePath);
        TransferObserver observer = sTransferUtility.upload(BUCKET_NAME, file.getName(),
                file);
        Toast.makeText(this, "File is not null",
                Toast.LENGTH_LONG).show();
        System.out.println("file name is: " + file.getName());
        System.out.println("file path is: " + filePath);
        System.out.println("file is: " + file);
        observer.setTransferListener(new TransferListener(){

            @Override
            public void onStateChanged(int id, TransferState state) {
                // do something
                Toast.makeText(getApplicationContext(), "onStateChanged",
                        Toast.LENGTH_LONG).show();
            }

            @Override
            public void onProgressChanged(int id, long bytesCurrent, long bytesTotal) {
                int percentage = (int) (bytesCurrent/bytesTotal * 100);
                //Display percentage transferred to user
                Toast.makeText(getApplicationContext(), "onProgressChanged",
                        Toast.LENGTH_LONG).show();
                System.out.println("percentage: "+percentage);
            }

            @Override
            public void onError(int id, Exception ex) {
                // do something
                Toast.makeText(getApplicationContext(), "onError",
                        Toast.LENGTH_LONG).show();
            }

        });
        /*
         * Note that usually we set the transfer listener after initializing the
         * transfer. However it isn't required in this sample app. The flow is
         * click upload button -> start an activity for image selection
         * startActivityForResult -> onActivityResult -> beginUpload -> onResume
         * -> set listeners to in progress transfers.
         */
        // observer.setTransferListener(new UploadListener());
    }
    /*
 * Gets the file path of the given Uri.
 */
    @SuppressLint("NewApi")
    private String getPath(Uri uri) throws URISyntaxException {
        final boolean needToCheckUri = Build.VERSION.SDK_INT >= 19;
        String selection = null;
        String[] selectionArgs = null;
        // Uri is different in versions after KITKAT (Android 4.4), we need to
        // deal with different Uris.
        if (needToCheckUri && DocumentsContract.isDocumentUri(getApplicationContext(), uri)) {
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                return Environment.getExternalStorageDirectory() + "/" + split[1];
            } else if (isDownloadsDocument(uri)) {
                final String id = DocumentsContract.getDocumentId(uri);
                uri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));
            } else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];
                if ("image".equals(type)) {
                    uri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    uri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }
                selection = "_id=?";
                selectionArgs = new String[] {
                        split[1]
                };
            }
        }
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = {
                    MediaStore.Images.Media.DATA
            };
            Cursor cursor = null;
            try {
                cursor = getContentResolver()
                        .query(uri, projection, selection, selectionArgs, null);
                int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
            }
        } else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }
        return null;
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }
}





/****ORIGINAL CODE**
package com.example.william.taps_basic;

        import android.content.Intent;
        import android.graphics.Bitmap;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.ImageView;

public class SendToCloud extends AppCompatActivity {

    private ImageView place_pic;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_to_cloud);

        Bitmap cameraImage = this.getIntent().getParcelableExtra("data");
        place_pic = (ImageView) findViewById(R.id.pic_to_cloud);
        place_pic.setImageBitmap(cameraImage);
    }

    public void sendtocloud(View view) {

    }

    public void return_to_camera_page(View view) {
        Intent go_to_camera_page = new Intent(this,SnapshotPage.class);
        startActivity(go_to_camera_page);

    }
}*/
