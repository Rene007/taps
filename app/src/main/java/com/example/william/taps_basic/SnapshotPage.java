package com.example.william.taps_basic;

import android.content.Intent;
import android.graphics.Bitmap;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class SnapshotPage extends AppCompatActivity {

    public static final int CAMERA_REQUEST = 6986;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapshot_page);
    }

    public void camera_page(View view) {
        Intent picIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(picIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap cameraImage = (Bitmap)data.getExtras().get("data");

            Intent pass_image = new Intent(this,SendToCloud.class);
            pass_image.putExtra("data", cameraImage);
            startActivity(pass_image);
        }
    }

    public void return_to_main(View view) {
        Intent go_to_main_page = new Intent(this,MainActivity.class);
        startActivity(go_to_main_page);
    }
}
//<uses-permission android:name="android.permission.CAMERA" />
//<uses-feature android:name="android.hardware.camera" android:required="true" />






/*ORIGINAL CODE:
public class SnapshotPage extends AppCompatActivity {

    public static final int CAMERA_REQUEST = 6986;
    private ImageView he_pic;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_snapshot_page);

        he_pic = (ImageView) findViewById(R.id.he_pic);
    }

    public void camera_page(View view) {
        Intent picIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(picIntent, CAMERA_REQUEST);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
            Bitmap cameraImage = (Bitmap)data.getExtras().get("data");

            he_pic.setImageBitmap(cameraImage);

            // Intent go_to_cloud_page = new Intent(this,SendToCloud.class);
            //startActivity(go_to_cloud_page);
            //data.putExtras(go_to_cloud_page,he_pic);
        }
    }
}*/