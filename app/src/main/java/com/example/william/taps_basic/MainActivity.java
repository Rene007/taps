package com.example.william.taps_basic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {

    //Pubnub pubnub = new Pubnub("pub-c-fd510598-85f0-4c44-bd8f-dfdd12416064", "sub-c-61c25e6a-c9e9-11e5-b684-02ee2ddab7fe");
    //Pubnub pubnub = new Pubnub("pub-c-b364700f-cf79-4ccd-95a7-880dfb4a2ebb", "sub-c-af52043a-05af-11e6-a6dc-02ee2ddab7fe");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void login(View view) {
        Intent navigation = new Intent(this,Navigation.class);
        startActivity(navigation);
    }

    public void take_tour(View view) {
        Intent intro = new Intent(this,Intro.class);
        startActivity(intro);
    }
}
