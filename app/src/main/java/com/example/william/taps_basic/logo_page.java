package com.example.william.taps_basic;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class logo_page extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_logo_page);
        }

    public void commence_page(View view) {
        Intent go_to_second_welcome_page = new Intent(this,MainActivity.class);
        startActivity(go_to_second_welcome_page);

/*        Intent go_to_snapshot_page = new Intent(this,SnapshotPage.class);
        startActivity(go_to_snapshot_page);*/

    }

    public void exitapp(View view) {
        finish();
    }
}