package com.example.william.taps_basic;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;
import com.pubnub.api.Callback;
import com.pubnub.api.Pubnub;
import com.pubnub.api.PubnubError;
import com.pubnub.api.PubnubException;

import java.util.ArrayList;
import java.util.List;

public class graph extends AppCompatActivity {



    Pubnub pubnub = new Pubnub("pub-c-fd510598-85f0-4c44-bd8f-dfdd12416064", "sub-c-61c25e6a-c9e9-11e5-b684-02ee2ddab7fe");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        int lambda_value = 1;

        List<DataPoint> values = new ArrayList<DataPoint>();

        for (int i = 0; i < 5; ++i) {
            values.add(new DataPoint(i, Math.exp(lambda_value * i)));
        }

        DataPoint[] projections = new DataPoint[values.size()];

        //must convert array to Datapoint
        for(int j = 0; j < values.size(); ++j) {
            projections[j] = values.get(j);
        }

        GraphView graph1 = (GraphView) findViewById(R.id.graph1);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(projections);
        graph1.addSeries(series);

        graph1.setTitle("Projected Tumor Growth");
        graph1.getGridLabelRenderer().setHorizontalAxisTitle("Time");
        graph1.getGridLabelRenderer().setVerticalAxisTitle("Tumor Size");

        try {
            pubnub.subscribe("my_channel", new Callback() {

                @Override
                public void connectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : CONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                @Override
                public void disconnectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : DISCONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                public void reconnectCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : RECONNECT on channel:" + channel
                            + " : " + message.getClass() + " : "
                            + message.toString());
                }

                @Override
                public void successCallback(String channel, Object message) {
                    System.out.println("SUBSCRIBE : " + channel + " : "
                            + message.getClass() + " : " + message.toString());
                    Intent navigation = new Intent(getApplicationContext(), test_graph.class);
                    navigation.putExtra("mydata", message.toString());
                    startActivity(navigation);
                    finish();
                }

                @Override
                public void errorCallback(String channel, PubnubError error) {
                    System.out.println("SUBSCRIBE : ERROR on channel " + channel
                            + " : " + error.toString());
                }
            });
        } catch (PubnubException e) {
            e.printStackTrace();
        }



    }
}