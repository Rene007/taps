package com.example.william.taps_basic;

import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.ArrayList;
import java.util.List;

public class test_graph extends AppCompatActivity {

    float lambda_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test_graph);
        Toolbar toolbar = (Toolbar) findViewById(R.id.graph_toolbar);
        setSupportActionBar(toolbar);

        android.support.v7.app.ActionBar mActionBar = getSupportActionBar();
        if (mActionBar != null) {
            mActionBar.setDisplayShowHomeEnabled(false);
        }
        if (mActionBar != null) {
            mActionBar.setDisplayShowTitleEnabled(false);
        }
        LayoutInflater mInflater = LayoutInflater.from(this);

        View mCustomView = mInflater.inflate(R.layout.snapshot_navbar, null);
        TextView mTitleTextView = (TextView) mCustomView.findViewById(R.id.snap_shot_text);
        mTitleTextView.setText("Graph");
        mTitleTextView.setTypeface(null, Typeface.BOLD);
        mTitleTextView.setGravity(Gravity.CENTER);

        mActionBar.setCustomView(mCustomView);
        mActionBar.setDisplayShowCustomEnabled(true);

        Intent intent = getIntent();
        String mydata = intent.getStringExtra("mydata");
        lambda_value = Float.parseFloat(mydata);

        List<DataPoint> values = new ArrayList<DataPoint>();

        for (int i = 0; i < 10; ++i) {
            values.add(new DataPoint(i, Math.exp(lambda_value * i)));
        }

        DataPoint[] projections = new DataPoint[values.size()];

        for(int j = 0; j < values.size(); ++j) {
            projections[j] = values.get(j);
        }

        GraphView graph1 = (GraphView) findViewById(R.id.graph2);

        LineGraphSeries<DataPoint> series = new LineGraphSeries<DataPoint>(projections);
        graph1.addSeries(series);

        graph1.setTitle("Projected Tumor Growth");
        graph1.getGridLabelRenderer().setHorizontalAxisTitle("Time");
        graph1.getGridLabelRenderer().setVerticalAxisTitle("Tumor Size");

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.snap_shot, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_name) {
            Intent data = new Intent();
            String text = "cancelled";
            data.setData(Uri.parse(text));
            setResult(RESULT_OK, data);
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
