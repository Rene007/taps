package com.example.william.taps_basic;

public interface NavigationDrawerCallbacks {
    void onNavigationDrawerItemSelected(int position);
}
